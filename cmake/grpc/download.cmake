cmake_minimum_required(VERSION 3.1)

project(grpc-download NONE)

include(ExternalProject)
ExternalProject_Add(grpc
  GIT_REPOSITORY    "https://github.com/grpc/grpc"
  GIT_TAG           "v1.38.0"
  SOURCE_DIR        "${GRPC_SRC}"
  BINARY_DIR        "${GRPC_BUILD}"
  CONFIGURE_COMMAND ""
  BUILD_COMMAND     ""
  INSTALL_COMMAND   ""
  TEST_COMMAND      ""
)